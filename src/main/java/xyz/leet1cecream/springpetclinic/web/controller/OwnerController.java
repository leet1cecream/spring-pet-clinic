package xyz.leet1cecream.springpetclinic.web.controller;

import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.leet1cecream.springpetclinic.persistance.model.Owner;
import xyz.leet1cecream.springpetclinic.service.OwnerService;

import java.util.Optional;

@Controller
@RequestMapping("/owners")
public class OwnerController {

    private final OwnerService ownerService;

    public OwnerController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @GetMapping
    public String list(Model model, Owner owner, Pageable pageable, BindingResult bindingResult) {

        if (owner.getLastName() == null) {
            owner.setLastName("");
        }

        Page<Owner> owners = ownerService.findByLastNameContaining(owner.getLastName(), pageable);

        if (owners.isEmpty()) {
            bindingResult.rejectValue("lastName", "notFound", "Not found");
            return "owners/findOwners";
        }

        if (owners.getTotalElements() == 1) {
            owner = owners.iterator().next();
            return "redirect:/owners/" + owner.getId();
        }

        model.addAttribute("ownersList", owners.getContent());
        model.addAttribute("currentPage", owners.getNumber() + 1);
        model.addAttribute("totalPages", owners.getTotalPages());
        model.addAttribute("totalItems", owners.getTotalElements());

        return "owners/ownersList";
    }

    @GetMapping("/{ownerId}")
    public String ownerDetails(Model model, @PathVariable Long ownerId) {

        Optional<Owner> ownerById = ownerService.findById(ownerId);

        model.addAttribute("owner", ownerById.get());

        return "owners/ownerDetails";
    }

    @GetMapping("/find")
    public String findOwner(Model model) {
        model.addAttribute("owner", new Owner());
        return "owners/findOwners";
    }

    @GetMapping("/new")
    public String newOwner(Model model) {
        model.addAttribute("owner", new Owner());
        return "owners/createOrUpdateOwner";
    }

    @PostMapping("/new")
    public String processNewOwner(Model model, @Valid Owner owner, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "owners/createOrUpdateOwner";
        }
        Owner savedOwner = ownerService.save(owner);
        return "redirect:/owners/" + savedOwner.getId();
    }

    @GetMapping("/{ownerId}/edit")
    public String editOwner(Model model, @PathVariable("ownerId") Long ownerId) {
        Optional<Owner> owner = ownerService.findById(ownerId);
        model.addAttribute("owner", owner.get());
        return "owners/createOrUpdateOwner";
    }

    @PostMapping("/{ownerId}/edit")
    public String processEditOwner(Model model, @Valid Owner owner, BindingResult bindingResult, @PathVariable("ownerId") Long ownerId) {
        if (bindingResult.hasErrors()) {
            return "owners/createOrUpdateOwner";
        }
        owner.setId(ownerId);
        Owner savedOwner = ownerService.save(owner);
        return "redirect:/owners/" + savedOwner.getId();
    }

}
