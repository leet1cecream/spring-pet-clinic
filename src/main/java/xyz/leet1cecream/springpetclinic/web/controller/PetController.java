package xyz.leet1cecream.springpetclinic.web.controller;

import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import xyz.leet1cecream.springpetclinic.persistance.model.Owner;
import xyz.leet1cecream.springpetclinic.persistance.model.Pet;
import xyz.leet1cecream.springpetclinic.persistance.model.PetType;
import xyz.leet1cecream.springpetclinic.service.OwnerService;
import xyz.leet1cecream.springpetclinic.service.PetService;

import java.util.Optional;

@Controller
@RequestMapping("/owners/{ownerId}")
public class PetController {

    private final PetService petService;
    private final OwnerService ownerService;

    public PetController(PetService petService, OwnerService ownerService) {
        this.petService = petService;
        this.ownerService = ownerService;
    }

    /*@GetMapping
    public String list(Model model, Pet pet, Pageable pageable, BindingResult bindingResult) {

        if (pet.getName() == null) {
            pet.setName("");
        }

        Page<Pet> pets = petService.findByNameContaining(pet.getName(), pageable);

        if (pets.isEmpty()) {
            bindingResult.rejectValue("name", "norFound", "Not Found");
            return "pets/petsFind";
        }

        model.addAttribute("petsList", pets.getContent());
        model.addAttribute("currentPage", pets.getNumber() + 1);
        model.addAttribute("totalPages", pets.getTotalPages());
        model.addAttribute("totalElements", pets.getTotalElements());

        return "pets/petsList";
    }*/

    @GetMapping("/pets/{petId}")
    public String petDetails(Model model, @PathVariable("petId") Long petId) {
        Optional<Pet> pet = petService.findById(petId);
        model.addAttribute("pet", pet.get());
        return "pets/petDetails";
    }

    @GetMapping("/pets/new")
    public String newPet(Model model, @PathVariable("ownerId") Long ownerId) {
        Pet pet = new Pet();
        pet.setPetType(new PetType());
        model.addAttribute("pet", pet);
        return "pets/createOrUpdatePet";
    }

    @PostMapping("/pets/new")
    public String processNewPet(Model model, @Valid Pet pet, BindingResult bindingResult, @PathVariable("ownerId") Long ownerId) {
        if (bindingResult.hasErrors()) {
            return "pets/createOrUpdatePet";
        }
        Owner owner = ownerService.findById(ownerId).get();
        Pet savedPet = petService.save(pet);

        savedPet.setOwner(owner);
        owner.addPet(savedPet);

        petService.save(savedPet);
        Owner savedOwner = ownerService.save(owner);

        return "redirect:/owners/" + savedOwner.getId();
    }

    @GetMapping("/pets/{petId}/edit")
    public String editPet(Model model, @PathVariable("ownerId") Long ownerId, @PathVariable("petId") Long petId) {
        Optional<Pet> pet = petService.findById(petId);
        model.addAttribute("pet", pet.get());
        return "pets/createOrUpdatePet";
    }

    @PostMapping("/pets/{petId}/edit")
    public String processEditPet(Model model, @Valid Pet pet, Owner owner, BindingResult bindingResult,
                                 @PathVariable("ownerId") Long ownerId, @PathVariable("petId") Long petId) {
        if (bindingResult.hasErrors()) {
            return "pets/createOrUpdatePet";
        }
        pet.setId(petId);
        Pet savedPet = petService.save(pet);
        return "redirect:/owners/" + ownerId;
    }

}
