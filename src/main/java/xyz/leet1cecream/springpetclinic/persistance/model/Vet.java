package xyz.leet1cecream.springpetclinic.persistance.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import xyz.leet1cecream.springpetclinic.persistance.model.mapped.Person;

import java.util.HashSet;
import java.util.Set;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "vets")
public class Vet extends Person {

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "vet_specialties", joinColumns = @JoinColumn(name = "vet_id"),
            inverseJoinColumns = @JoinColumn(name = "specialty_id"))
    Set<Specialty> specialties = new HashSet<>();

    @OneToMany(mappedBy = "vet", fetch = FetchType.EAGER, orphanRemoval = true)
    Set<Visit> visits = new HashSet<>();
}
