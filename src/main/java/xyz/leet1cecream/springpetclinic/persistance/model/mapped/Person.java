package xyz.leet1cecream.springpetclinic.persistance.model.mapped;

import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

@Getter @Setter @ToString(callSuper = true)
@MappedSuperclass
public class Person extends BaseEntity{

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

}
