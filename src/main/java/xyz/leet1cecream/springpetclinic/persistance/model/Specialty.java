package xyz.leet1cecream.springpetclinic.persistance.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.NoArgsConstructor;
import xyz.leet1cecream.springpetclinic.persistance.model.mapped.NamedEntity;

@NoArgsConstructor
@Entity
@Table(name = "specialties")
public class Specialty extends NamedEntity {
}
