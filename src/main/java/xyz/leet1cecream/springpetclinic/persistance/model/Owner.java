package xyz.leet1cecream.springpetclinic.persistance.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import xyz.leet1cecream.springpetclinic.persistance.model.mapped.Person;

import java.util.HashSet;
import java.util.Set;

@Getter @Setter @NoArgsConstructor @ToString(callSuper = true)
@Entity
@Table(name = "owners")
public class Owner extends Person {

    @NotEmpty
    private String address;

    @NotEmpty
    private String city;

    @NotEmpty
    private String telephone;

    @JsonManagedReference
    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Pet> pets = new HashSet<>();

    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Visit> visits = new HashSet<>();

    public void addPet(Pet pet) {
        if (pet.getId() == null) {
            pets.add(pet);
        }
    }

}
