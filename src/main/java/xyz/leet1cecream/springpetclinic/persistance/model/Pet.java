package xyz.leet1cecream.springpetclinic.persistance.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.Past;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import xyz.leet1cecream.springpetclinic.persistance.model.mapped.NamedEntity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter @Setter @NoArgsConstructor @ToString(callSuper = true)
@Entity
@Table(name = "pets")
public class Pet extends NamedEntity {

    @Column(name = "birth_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Past
    private LocalDate birthDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "type_id")
    private PetType petType;

    @ToString.Exclude
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "owner_id")
    Owner owner;

    @OneToMany(mappedBy = "pet", fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<Visit> visits = new HashSet<>();
}
