package xyz.leet1cecream.springpetclinic.persistance.model.mapped;

import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString(callSuper = true)
@MappedSuperclass
public class NamedEntity extends BaseEntity {

    @NotEmpty
    private String name;

}
