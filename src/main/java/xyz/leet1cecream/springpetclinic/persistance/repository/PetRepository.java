package xyz.leet1cecream.springpetclinic.persistance.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import xyz.leet1cecream.springpetclinic.persistance.model.Pet;

@Repository
public interface PetRepository extends CrudRepository<Pet, Long>, PagingAndSortingRepository<Pet, Long>  {

    Page<Pet> findByNameContaining(String name, Pageable pageable);

}
