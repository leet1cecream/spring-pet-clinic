package xyz.leet1cecream.springpetclinic.persistance.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import xyz.leet1cecream.springpetclinic.persistance.model.Owner;

@Repository
public interface OwnerRepository extends CrudRepository<Owner, Long>, PagingAndSortingRepository<Owner, Long> {

    Iterable<Owner> findByLastName(String name);
    Page<Owner> findByLastNameContaining(String name, Pageable pageable);

}
