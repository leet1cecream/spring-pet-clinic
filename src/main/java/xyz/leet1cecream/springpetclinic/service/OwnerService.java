package xyz.leet1cecream.springpetclinic.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import xyz.leet1cecream.springpetclinic.persistance.model.Owner;

import java.util.Optional;

public interface OwnerService {

    Owner save(Owner owner);
    Iterable<Owner> findAll();
    Page<Owner> findByLastNameContaining(String name, Pageable pageable);
    Optional<Owner> findById(Long id);

}
