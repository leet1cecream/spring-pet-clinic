package xyz.leet1cecream.springpetclinic.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xyz.leet1cecream.springpetclinic.persistance.model.Owner;
import xyz.leet1cecream.springpetclinic.persistance.repository.OwnerRepository;
import xyz.leet1cecream.springpetclinic.service.OwnerService;

import java.util.Optional;

@Service
public class OwnerServiceImpl implements OwnerService {

    private final OwnerRepository ownerRepository;

    OwnerServiceImpl(OwnerRepository ownerRepository) {
        this.ownerRepository = ownerRepository;
    }


    @Override
    public Owner save(Owner owner) {
        return ownerRepository.save(owner);
    }

    @Override
    public Iterable<Owner> findAll() {
        return ownerRepository.findAll();
    }

    @Override
    public Page<Owner> findByLastNameContaining(String name, Pageable pageable) {
        return ownerRepository.findByLastNameContaining(name, pageable);
    }

    @Override
    public Optional<Owner> findById(Long id) {
        return ownerRepository.findById(id);
    }
}
