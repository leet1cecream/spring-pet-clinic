package xyz.leet1cecream.springpetclinic.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xyz.leet1cecream.springpetclinic.persistance.model.Pet;
import xyz.leet1cecream.springpetclinic.persistance.repository.PetRepository;
import xyz.leet1cecream.springpetclinic.service.PetService;

import java.util.Optional;

@Service
public class PetServiceImpl implements PetService {

    private final PetRepository petRepository;

    public PetServiceImpl(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @Override
    public Pet save(Pet pet) {
        return petRepository.save(pet);
    }

    @Override
    public Iterable<Pet> findAll() {
        return petRepository.findAll();
    }

    @Override
    public Page<Pet> findByNameContaining(String name, Pageable pageable) {
        return petRepository.findByNameContaining(name, pageable);
    }

    @Override
    public Optional<Pet> findById(Long id) {
        return petRepository.findById(id);
    }
}
