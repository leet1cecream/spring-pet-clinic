package xyz.leet1cecream.springpetclinic.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import xyz.leet1cecream.springpetclinic.persistance.model.Pet;

import java.util.Optional;

public interface PetService {
    Pet save(Pet Pet);
    Iterable<Pet> findAll();
    Page<Pet> findByNameContaining(String name, Pageable pageable);
    Optional<Pet> findById(Long id);
}
